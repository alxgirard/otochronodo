from urllib.request import urlopen
import json
from datetime import datetime, date, timedelta
import time
import beepy

locations = [
    {
        "title": "compans",
        "url": "https://booking.keldoc.com/api/patients/v2/timetables/96873?from={from_date}&to={to_date}&agenda_ids%5B%5D=53154&agenda_ids%5B%5D=53155&agenda_ids%5B%5D=53156&agenda_ids%5B%5D=53157&agenda_ids%5B%5D=53158&agenda_ids%5B%5D=53159&agenda_ids%5B%5D=53160&agenda_ids%5B%5D=53161&agenda_ids%5B%5D=53162&agenda_ids%5B%5D=53163&agenda_ids%5B%5D=53164&agenda_ids%5B%5D=53165&agenda_ids%5B%5D=53166&agenda_ids%5B%5D=53167&agenda_ids%5B%5D=53168&agenda_ids%5B%5D=53169&agenda_ids%5B%5D=53170&agenda_ids%5B%5D=53171&agenda_ids%5B%5D=53172&agenda_ids%5B%5D=53174&agenda_ids%5B%5D=53175&agenda_ids%5B%5D=53176&agenda_ids%5B%5D=53177&agenda_ids%5B%5D=53178&agenda_ids%5B%5D=53179"
    },
    {
        "title": "bourbaki",
        "url": "https://booking.keldoc.com/api/patients/v2/timetables/97164?from={from_date}&to={to_date}&agenda_ids%5B%5D=49004&agenda_ids%5B%5D=50002&agenda_ids%5B%5D=53260&agenda_ids%5B%5D=53261"
    }
]
date_interval = timedelta(4)
loop_interval = 60.0

import logging

class CustomFormatter(logging.Formatter):
    """Logging Formatter to add colors and count warning / errors"""

    grey = "\x1b[38;21m"
    yellow = "\x1b[33;21m"
    red = "\x1b[31;21m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"
    format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s (%(filename)s:%(lineno)d)"

    FORMATS = {
        logging.DEBUG: grey + format + reset,
        logging.INFO: grey + format + reset,
        logging.WARNING: yellow + format + reset,
        logging.ERROR: red + format + reset,
        logging.CRITICAL: bold_red + format + reset
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)

# create logger with 'spam_application'
logger = logging.getLogger("otochronodo")
logger.setLevel(logging.DEBUG)
logging.basicConfig(filename="otochronodo.log")

# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

ch.setFormatter(CustomFormatter())

logger.addHandler(ch)

while True:

    for location in locations:

        today_date = date.today()
        today_url = location["url"].format(
            from_date = today_date,
            to_date = today_date + date_interval
        )

        data = json.loads(urlopen(today_url).read().decode("utf-8"))

        output = {
            "location": location["title"],
            "availabilities": {},
            "timestamp": datetime.now().isoformat(),
            "source": data
        }

        if "availabilities" in data:
            output["availabilities"] = data["availabilities"]
            beepy.beep(sound="coin")
            logger.critical(output)
        else:
            logger.info(output)

    time.sleep(loop_interval - time.monotonic() % loop_interval)
